# HsWarChess

## Run
```shell
yarn install
yarn dev
```

Visit http://localhost:8081/

## Dependency
### Graphic render
https://pixijs.com/
### Tween
https://greensock.com/docs/v3/GSAP
### Easing functions
https://easings.net
