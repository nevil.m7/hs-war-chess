import {LinkedList} from "../src/common/util/linked-node";

const list = new LinkedList<number>();
list.append(0, 1, 2, 3, 4, 5, 6, 7, 8);
console.log(list.toArray());

list.remove(3);
console.log(list.toArray());
