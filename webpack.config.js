const path = require('path')
module.exports = {
    mode: "development",
    entry: './src/main.ts',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: "[name]-bundle.js"
    },
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ['ts-loader']
            }
        ]
    },
    devServer: {
        static: {
            directory: path.join(__dirname, './build'),
        },
        compress: true,
        host: "localhost",
        port: 8081
    },
    externals: {
        // "pixi.js": "PIXI"
    }
}