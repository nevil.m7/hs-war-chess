import * as PIXI from 'pixi.js'
import {ChessBoard} from "./view/chess-board";

export class Game {
    public static app: PIXI.Application;
    public static ticker: PIXI.Ticker;
    public static stage: PIXI.Container;
    public static chessBoard: ChessBoard;
}