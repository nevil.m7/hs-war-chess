export abstract class BaseModel {

    private _proxy: this;

    get proxy() {
        if (!this._proxy) {
            this._proxy = new Proxy(this, {});
        }
        return this._proxy
    }
}