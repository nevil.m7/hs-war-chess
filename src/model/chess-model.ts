import {Skill} from "../skill/skill";
import {BaseModel} from "./base-model";

export class ChessModel extends BaseModel {
    public hp: number;
    public atk: number;
    private skills: Skill[];

    constructor(
        public readonly initHp: number,
        public readonly initAtk: number,
    ) {
        super();

        this.hp = this.initHp;
        this.atk = this.initAtk;
        this.skills = [];
    }

    public isDead() {
        return this.hp <= 0;
    }

    public addAtk(value: number) {
        this.atk += value;
    }

    public addHp(value: number) {
        this.hp += value;
    }

}
