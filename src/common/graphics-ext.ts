import {Graphics} from "@pixi/graphics"
import {Area, Point} from "./2d";

export class GraphicsExt extends Graphics {
    public drawDash(p1: Point, p2: Point, dashLength = 5, spaceLength = 5) {
        let x = p2.x - p1.x;
        let y = p2.y - p1.y;
        let hyp = Math.sqrt((x) * (x) + (y) * (y));
        let units = hyp / (dashLength + spaceLength);
        let dashSpaceRatio = dashLength / (dashLength + spaceLength);
        let dashX = (x / units) * dashSpaceRatio;
        let spaceX = (x / units) - dashX;
        let dashY = (y / units) * dashSpaceRatio;
        let spaceY = (y / units) - dashY;
        this.moveTo(p1.x, p1.y);
        while (hyp > 0) {
            p1.x += dashX;
            p1.y += dashY;
            hyp -= dashLength;
            if (hyp < 0) {
                p1.x = p2.x;
                p1.y = p2.y;
            }
            this.lineTo(p1.x, p1.y);
            p1.x += spaceX;
            p1.y += spaceY;
            this.moveTo(p1.x, p1.y);
            hyp -= spaceLength;
        }
        this.moveTo(p2.x, p2.y);
    }

    public drawRoundedRectArea(area: Area, radius: number) {
        this.drawRoundedRect(area.x, area.y, area.width, area.height, radius);
    }
}