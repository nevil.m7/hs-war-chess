import {Runner} from '@pixi/runner'

const runners = {};

function getRunner(name: string): Runner {
    if (!runners[name]) {
        console.log('new Runners', name);
        runners[name] = new Runner(name);
    }
    return runners[name];
}

export async function emit(name, ...params) {
    await getRunner(name).emit(...params);
}

export async function collect(item, name) {
    await getRunner(name).add(item);
}
