
export interface Point {
    x: number;
    y: number;
}

export interface Rect {
    width: number;
    height: number;
}

export interface Area extends Point, Rect {}