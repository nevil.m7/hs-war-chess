import {Expo, gsap} from "gsap";

export class Animation {
    public static shake(item, times: number = 30, effect = 20) {
        return gsap.from(item, {
            duration: 0.03,
            repeat: times - 1,
            y: item.y + (1 + Math.random() * effect),
            x: item.x + (1 + Math.random() * effect),
            delay: 0.1,
            ease: Expo.easeInOut,
            yoyo: true
        });
        // return gsap.to(item,{duration: 0.05, y:item.y, x:item.x, ease:Expo.easeInOut}).then();
    }
}

export const Back = {
    easeIn(x: number): number {
        // const c1 = 1.70158;
        const c1 = 3;
        const c3 = c1 + 1;
        return c3 * x * x * x - c1 * x * x;
    },
    easeOut(x: number): number {
        // const c1 = 1.70158;
        const c1 = 3;
        const c3 = c1 + 1;
        return 1 + c3 * Math.pow(x - 1, 3) + c1 * Math.pow(x - 1, 2);
    }
}

export const Elastic = {
    easeOut(x: number): number {
        const c4 = (4 * Math.PI) / 3;
        if (x === 0) return 0;
        if (x === 1) return 1;
        return Math.pow(1.3, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
    }
}