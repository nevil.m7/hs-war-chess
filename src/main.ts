import * as PIXI from 'pixi.js'
import {ChessBoard} from './view/chess-board'
import {ChessBoardConfig} from "./const";
import {Game} from "./game";
import {gsap} from "gsap";

Game.app = new PIXI.Application({
    backgroundColor: 0x444444,
    width: ChessBoardConfig.width * 1.2,
    height: ChessBoardConfig.height * 1.2,
});
Game.ticker = Game.app.ticker;
Game.stage = Game.app.stage;

document.body.appendChild(Game.app.view);

Game.chessBoard = new ChessBoard();
Game.chessBoard.x = 0.1 * ChessBoardConfig.width;
Game.chessBoard.y = 0.1 * ChessBoardConfig.height;
Game.app.stage.addChild(Game.chessBoard);

start();

async function start() {
    await gsap.from(Game.chessBoard, {duration: 1, alpha: 0})
    await Game.chessBoard.showAttack();
}
