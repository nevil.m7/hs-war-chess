export enum SkillType {
    DEATH_RATTLE,     // 亡语
    REBORN,           // 复生
    ATK_EFFECT,       // 攻击特效
    POISON,           // 剧毒
    // REVENGE,          // 复仇
}