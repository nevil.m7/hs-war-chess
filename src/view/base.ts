import {Sprite} from '@pixi/sprite'
import {BaseModel} from "../model/base-model";
import {GraphicsExt as Graphics} from "../common/graphics-ext";

export abstract class Base extends Sprite {

    public proxy: this;
    protected bg !: Graphics;

    constructor(model ?: BaseModel) {
        super();
        this.proxy = new Proxy(this, this.getProxyHandler())
        this.init();
        if (model) {
            this.proxy._model = model;
        }
    }

    protected _model !: BaseModel;

    public get model(): BaseModel {
        return this.proxy._model;
    }

    public set model(model: BaseModel) {
        this.proxy._model = model;
    }

    abstract get w(): number;

    abstract get h(): number;

    abstract init(): void;

    abstract clone(): Base;

    abstract getProxyHandler(): ProxyHandler<this>

    abstract set(target: this, p: string | symbol, value: any, receiver: any): boolean
}