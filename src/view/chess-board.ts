import {Container} from '@pixi/display'
import {Text, TextStyle} from '@pixi/text';
import {GraphicsExt as Graphics} from "../common/graphics-ext";
import {Point} from "../common/2d";
import {Chess} from "./chess";
import {ChessModel} from "../model/chess-model";
import {ChessBoardConfig as Config} from "../const";
import {ChessBoardConfig} from "../config/chess-board-config";
import {collect} from "../common/runner-factory";
import {gsap} from "gsap";
import {Back, Elastic} from "../common/animation";
import {Battle} from "../abstract/battle";
import {ChessTeam} from "../abstract/chess-team";

export class ChessBoard extends Container {
    private static readonly config: ChessBoardConfig = new ChessBoardConfig(Config);

    private chessTeams = {};

    private bgLayer: Graphics;
    private chessLayer: Container;
    private effectLayer: Container;

    constructor() {
        super();

        this.initVars();
        this.initLayers();
        this.initChessTeams();
        this.initEvents();
    }

    static get center(): Point {
        return this.config.center;
    }

    private static drawBg(): Graphics {
        const bg = new Graphics();
        bg.beginFill(Config.bgColor);
        bg.drawRoundedRect(0, 0, Config.width, Config.height, 50);
        bg.endFill();

        bg.lineStyle({
            width: 10,
            alignment: 0.5,
            color: 0xff0000,
            // cap: LINE_CAP.BUTT,
        });
        bg.drawDash({x: 0, y: ChessBoard.center.y}, {x: Config.width, y: ChessBoard.center.y}, 20, 15);

        bg.lineStyle({width: 2, color: 0xff0000});
        bg.beginFill(0x495634);
        bg.drawRoundedRectArea(ChessBoard.config.chessUpArea, 20);
        bg.drawRoundedRectArea(ChessBoard.config.chessDownArea, 20);
        bg.endFill();

        return bg;
    }

    public addChess(team: string, chess: Chess) {
        const cTeam = this.chessTeams[team] as ChessTeam;
        chess.team = team;
        cTeam.addChess(chess);

        this.chessLayer.addChild(chess);
        // this.sortChessTeam(team);
    }

    public removeChess(chess: Chess) {
        const chessTeam = this.chessTeams[chess.team] as ChessTeam;
        chessTeam.remove(chess);
    }

    public async showAttack() {
        const upTeam = this.chessTeams['up'] as ChessTeam;
        const downTeam = this.chessTeams['down'] as ChessTeam;

        const battle = new Battle(upTeam, downTeam);
        await battle.start();
    }

    public async shake() {
        const x = 20 + 20 * Math.random();
        const y = 20 + 20 * Math.random();
        gsap.from(this, {duration: .2, x, y, ease: Elastic.easeOut})
    }

    async chessDie(chess: Chess) {
        this.removeChess(chess);
    }

    async showDamage(chess: Chess, damage: number) {
        const style = new TextStyle({
            fontFamily: 'Arial',
            fontSize: 45,
            fontWeight: 'bold',
            stroke: '#000000',
            strokeThickness: 5,
            fill: ['#ffffff'],
        })

        const dmgStr = '-' + damage.toString(10);
        const txt = new Text(dmgStr, style);
        txt.x = chess.x;
        txt.y = chess.y;
        // txt.alpha = 0.8;
        txt.anchor.set(0.5);
        this.effectLayer.addChild(txt)

        await gsap.from(txt, {alpha: 0, width: 0, height: 0, ease: Back.easeOut})
        await gsap.to(txt, {alpha: 0, delay: 0.5})

        this.effectLayer.removeChild(txt)
        txt.destroy(true)
    }

    private initLayers() {
        this.bgLayer = ChessBoard.drawBg();
        this.addChild(this.bgLayer)

        this.chessLayer = new Container();
        this.chessLayer.sortableChildren = true;
        this.addChild(this.chessLayer);

        this.effectLayer = new Container();
        this.addChild(this.effectLayer);
    }

    private initVars() {}

    private initChessTeams() {
        this.chessTeams['up'] = new ChessTeam('up', ChessBoard.config.chessUpArea);
        this.chessTeams['down'] = new ChessTeam('down', ChessBoard.config.chessDownArea);

        for (let i = 0; i < 7; i++) {
            const chess = new Chess();
            const atk = Math.ceil(Math.random() * 100);
            const hp = Math.ceil(Math.random() * 100);
            chess.proxy.model = new ChessModel(hp, atk);
            this.addChess('up', chess);
        }

        for (let i = 0; i < 7; i++) {
            const chess = new Chess();
            const atk = Math.ceil(Math.random() * 100);
            const hp = Math.ceil(Math.random() * 100);
            chess.proxy.model = new ChessModel(hp, atk);
            this.addChess('down', chess);
        }
    }

    private initEvents() {
        collect(this, 'afterAttack');
        collect(this, 'chessDie');
        collect(this, 'showDamage');
    }

    private afterAttack() {
        this.shake();
    }
}