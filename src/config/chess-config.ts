import {Config} from "./config";

export interface IChessConfig extends Config {
    width: number,
    height: number
}

export class ChessConfig implements IChessConfig {
    public width: number;
    public height: number;

    constructor(config: IChessConfig) {
        this.width = config.width;
        this.height = config.height;
    }
}