import {Config} from "./config";
import {Area, Point} from "../common/2d";

export interface IChessBoardConfig extends Config {
    width: number,
    height: number,
    marginX: number,
    marginY: number,
    chessMargin: number,
    bgColor: number,
}

export class ChessBoardConfig implements IChessBoardConfig {
    public width: number;
    public height: number;
    public bgColor: number;
    public chessMargin: number;
    public marginX: number;
    public marginY: number;

    private config: IChessBoardConfig;

    constructor(config: IChessBoardConfig) {
        this.width = config.width;
        this.height = config.height;
        this.bgColor = config.bgColor;
        this.chessMargin = config.chessMargin;
        this.marginX = config.marginX;
        this.marginY = config.marginY;

        this.config = config;
    }

    get center(): Point {
        return {
            x: this.width / 2,
            y: this.height / 2,
        }
    }

    get chessUpArea(): Area {
        return {
            x: this.marginX,
            y: this.marginY,
            width: this.width - this.marginX * 2,
            height: this.center.y - this.marginY * 2,
        }
    }

    get chessDownArea(): Area {
        return {
            x: this.marginX,
            y: this.center.y + this.marginY,
            height: this.center.y - this.marginY * 2,
            width: this.width - this.marginX * 2,
        }
    }

}