import {LinkedList, LinkedNode} from "../common/util/linked-list";
import {Chess} from "../view/chess";
import {ChessBoardConfig as Config, ChessStatus} from "../const";
import {Area} from "../common/2d";
import {gsap} from "gsap";

export class ChessTeam {
    public list: LinkedList<Chess>
    private atkNode: LinkedNode<Chess>

    constructor(
        public key: string,
        public area: Area,
        chessArr?: Array<Chess>
    ) {
        this.list = new LinkedList<Chess>();
        if (chessArr) {
            this.list.append(...chessArr);
            this.init();
        }
    }

    init() {
        this.atkNode = this.list.head;
    }

    public isEmpty() {
        return this.list.length == 0;
    }

    public isDead() {
        if (!this.list) return false;
        return this.list.length <= 0;
    }

    public addChess(...arr: Array<Chess>) {
        const isEmpty = this.list.length == 0;
        this.list.append(...arr);
        if (isEmpty) {
            this.atkNode = this.list.head;
        }
        this.sort();
    }

    public win() {
        console.log(`Team ${this.key} win!!!`);
    }

    public getDefender() {
        const i = Math.floor(Math.random() * this.list.length);
        console.log(this.key, 'Defender ====', i, 'length=',this.list.length);
        return this.list.get(i).item;
    }

    public async attack(team: ChessTeam) {
        const target = team.getDefender();
        target.status = ChessStatus.DEFEND;
        await this.atkNode.item.attack(target);
        this.setNext();
    }

    public remove(chess: Chess) {
        const node = this.list.find(chess);
        this.list.remove(chess);
        chess.remove();

        if (chess.status == ChessStatus.DEFEND && node == this.atkNode) {
            this.setNext();
        }
    }

    private setNext() {
        if (!this.atkNode) return;
        console.log(this.key, 'setNext');

        this.atkNode = this.atkNode.next;
        if (!this.atkNode) this.atkNode = this.list.head;
    }

    public async sort() {
        const list = this.list;
        const area = this.area;

        const y = area.y + area.height / 2;
        const count = list.length;
        const teamWidth = Chess.WIDTH * count + Config.chessMargin * (count - 1);

        const chessArr = list.toArray();
        const promises = chessArr.map(async (chess, i) => {
            const x = area.x + (area.width - teamWidth + chess.w) / 2 + i++ * (chess.w + Config.chessMargin);
            return gsap.to(chess, {x, y, duration: .2});
        });
        return await Promise.all(promises);
    }
}