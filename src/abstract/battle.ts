import {ChessTeam} from "./chess-team";

export class Battle {
    private flag: boolean;

    constructor(
        public teamA: ChessTeam,
        public teamB: ChessTeam
    ) {
        this.flag = Math.random() < 0.5;
    }

    public async start() {
        while (!this.isEnd()) {
            await this.sort();
            this.flag ? await this.teamA.attack(this.teamB) : await this.teamB.attack(this.teamA);
            this.flag = !this.flag;
        }
        this.sort();
        console.log('END!!!!!!!!');
    }

    private isEnd() {
        if (this.teamA.isDead() && !this.teamB.isDead()) {
            this.teamB.win();
            return true;
        } else if (this.teamB.isDead() && !this.teamA.isDead()) {
            this.teamA.win();
            return true;
        } else if (this.teamA.isDead() && this.teamB.isDead()) {
            console.log('Draw!');
            return true;
        }
        return false
    }

    public setWinner(team:ChessTeam) {

    }

    private async sort() {
        await Promise.all([
            this.teamA.sort(),
            this.teamB.sort(),
        ]);
    }

}