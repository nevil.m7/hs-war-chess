export enum EventType {}

export const ChessBoardConfig = {
    width: 1000,
    height: 800,
    marginX: 100,
    marginY: 80,
    chessMargin: 10,
    bgColor: 0x1099bb,
};

export const ChessConfig = {
    width: 100,
    height: 160,
};

export const Color = {
    GREEN: "#00ff00",
    RED: "#ff0000",
    WHITE: "#ffffff",
};

export enum ChessStatus {
    WAITING,
    ATTACK,
    DEFEND,
}
